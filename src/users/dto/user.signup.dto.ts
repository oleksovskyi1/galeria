import { IsNotEmpty, IsString, IsEmail, MinLength, MaxLength, Validate } from 'class-validator'
import { IsEmailUnique } from "../users-validator.decorator";

export class UserSignupDto {
    @IsNotEmpty()
    @IsString()
    readonly name: string

    @IsNotEmpty()
    @IsEmail({}, { message: 'Please enter correct Email' })
    @Validate(IsEmailUnique)
    readonly email: string

    @IsNotEmpty()
    @IsString()
    @MinLength(6)
    @MaxLength(50)
    readonly password: string
}
