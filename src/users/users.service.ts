import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './user.model';
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from 'bcryptjs'
import {UserSignupDto} from "./dto/user.signup.dto";


@Injectable()
export class UsersService {

    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>,
        private jwtService: JwtService
    ) {}

    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async findById(id: string): Promise<User> {
        return this.userModel.findById(id).exec();
    }

    async create(user: UserSignupDto): Promise<{ token: string }> {
        const { name, email, password } = user

        const saltRounds = 10
        const hash = await bcrypt.hash(password, saltRounds)

        const createdUser = new this.userModel({
            name,
            email,
            password: hash
        });
        createdUser.save()

        const token = this.jwtService.sign({ id: createdUser._id })

        return { token }
    }

    async findByEmail(email: string): Promise<User> {
        return this.userModel.findOne({ email }).exec();
    }

    async update(id: string, user: User): Promise<User> {
        return this.userModel.findByIdAndUpdate(id, user, { new: true }).exec();
    }

    async remove(id: string): Promise<User> {
        return this.userModel.findByIdAndRemove(id).exec();
    }
}
