import {Controller, Param, Body, Get, Post, Put, Delete} from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from "./user.model";
import { UserSignupDto } from "./dto/user.signup.dto";

@Controller('users')
export class UsersController {

    constructor(private readonly usersService: UsersService) {}

    @Get()
    async findAll(): Promise<User[]> {
        return this.usersService.findAll();
    }

    @Get(':id')
    async findById(@Param('id') id: string): Promise<User> {
        return this.usersService.findById(id);
    }

    @Post()
    async create(@Body() user: UserSignupDto): Promise<{ token: string }> {
        return this.usersService.create(user);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() user: User): Promise<User> {
        return this.usersService.update(id, user);
    }

    @Delete(':id')
    async remove(@Param('id') id: string): Promise<User> {
        return this.usersService.remove(id);
    }
}
