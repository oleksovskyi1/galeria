import { Module } from '@nestjs/common';
import { JwtModule } from "@nestjs/jwt";
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User, UserSchema } from './user.model';
import { PassportModule } from "@nestjs/passport";
import { IsEmailUnique } from "./users-validator.decorator";

@Module({
  imports: [
      PassportModule.register({ defaultStrategy: 'jwt' }),
      JwtModule.registerAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: () => {
              return {
                  secret: process.env.SECRET_KEY,
                  signOptions: {
                      expiresIn: process.env.EXPIRE_TIME,
                  },
              }
          }
      }),
      MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])
  ],
  controllers: [UsersController],
  providers: [IsEmailUnique, UsersService]
})
export class UsersModule {}
