import { ConflictException, Injectable} from '@nestjs/common';
import {ValidationArguments, ValidationError, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import { HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';

@Injectable()
@ValidatorConstraint({ name: 'isEmailUnique', async: true })
export class IsEmailUnique implements ValidatorConstraintInterface {

    constructor(private readonly usersService: UsersService) {}

    async validate(email: string, args: ValidationArguments) {
        const user = await this.usersService.findByEmail(email);

        if (user) {
            const message = 'Email already exist!';
            throw new ConflictException(message);
        }

        return true;
    }
}
